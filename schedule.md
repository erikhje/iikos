[[_TOC_]]

# About the Course

## Expected background

We will use Linux command line and C-programming in this course. You can solve most exercises by simply logging in (ssh)
to `login.stud.ntnu.no` (or using a virtual Linux on your own laptop), but you
will also be given access to NTNUs private cloud SkyHiGh to create your own
private Linux virtual machine (see [instructions](https://gitlab.com/erikhje/dcsg1005/blob/master/heat-labs.md) and [video lecture](https://youtu.be/CBtF4F70PN4) (note: the yaml-file to use is not correct in the video, use the one called `single_linux.yaml` instead of `single_linux_20.04.yaml`).

* BIDATA know Java-programming and some Linux and C from IIKG1003.
* BPROG know C-programming and some Linux from IIKG1001.
* DIGSEC know C-programming and Linux from DCSG1001.

## Course setup

| Time&Place | Happening |
| ---- | ------------- |
| Tuesdays 0815-1000 2/3 Eureka | Lecture ALL with [Erik](https://www.ntnu.no/ansatte/erik.hjelmas) |
| **Week 37,38,40,41,43,44: Tuesday 1615-1800 S410**  | **Approval of mandatory exercises with Teaching Assistants** |
| Thursdays 1615-1800 S410 (T119 in week 41) | Lab/Exercises DIGSEC with Teaching Assistants |
| Thursdays 1615-1800 S411 (S410 in week 37) | Lab/Exercises BPROG with Teaching Assistants |
| Thursdays 1615-1800 S415 (S410 in week 39)  | Lab/Exercises BIDATA with Teaching Assistants |
| Fridays 0815-1000 S206 (Wednesday last two weeks) | Q&A-session ALL with Erik |

([See tp](https://tp.educloud.no/ntnu/timeplan/?id%5B%5D=IDATG2202%2C1&type=course&week=34&weekTo=47&ar=2024&campus=&hide_old=1) for official schedule of course).
<!--
Every week we have two hours of lectures plus one additional session for CISK at Jørstadmoen and one additional session for NTNU-students. In addition the teaching assistants are available for two hours per week in class rooms plus **"always" available in Piazza**. Lectures will not be recorded, but last years lectures are [available in Omnom](https://forelesning.gjovik.ntnu.no/publish/index.php?lecturer=all&topic=IDATG2202+Operativsystemer). In addition there will sometimes be video lectures available in the weekly schedule below.

## Mandatory assignments

**There will be three [team-based learning (TBL)](https://www.teambasedlearning.org/definition) sessions. You have to attend and actively participate in at least two of them to qualify for the exam**.
 TBL means that we have a two hour session where we do the following in sequence:

1. Individual Readiness Assurance Test (iRAT)
2. Team Readiness Assurance Test (tRAT)
3. Clarification session
4. Application exercises

The questions you get in iRAT/tRAT are in the same format and complexity level that you will get on some
parts of the final exam.
-->
## Download course material

* [All code examples from the text book](https://github.com/remzi-arpacidusseau/ostep-code)
* [All code mentioned in the homework in the text book](https://github.com/remzi-arpacidusseau/ostep-homework)
* [All your teacher's additional demo files used in lectures](https://gitlab.com/erikhje/iikos-files)

## How to spend your 10-12 hours allocated for this course to get a good grade

Recommended sequence:

1. Read the texts assigned in the weekly schedule below (2hrs).
2. Watch the weekly lectures in Panopto (in Blackboard) or on [Youtube](https://www.youtube.com/playlist?list=PLmiI5VoyQBWOMU-wMo60bT_mGgvMYnerP) (1hr).
3. Participate in the teaching on Tuesdays (2hrs).
4. Do the exercises "Review questions and problems" in [the
   compendia](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.pdf)
   (and "Lab tutorials" if they exist) in the lab hours with the teaching
   assistants (2hrs).
5. Self-studies (Tuesday-Menti, "Review questions and problems" or read
   assigned texts) (1-3hrs)
6. Friday 8-10 (exercise solutions, Q&A) (2t)

Tuesdays we spend doing exercises in Menti together where you can ask
questions as we go along, it is very important that you have read the
assigned text and watched the lectures in Panopto or on
[YouTube](https://www.youtube.com/playlist?list=PLmiI5VoyQBWOMU-wMo60bT_mGgvMYnerP)
before the Tuesday sessions. The Friday sessions are for sample solutions
to whatever exercises you would like me to solve, and maybe we revisit
topics from Tuesday where we were short on time, and we will have an open
Q&A-session.

## What if I am sick or cannot study for a week or two (or three)?

Don't worry, the topics in this course have a natural sequence, but there
is not a strong dependence between each week. If you have been unable to
study for some time, just jump into the topics the current week and go back
and revisit previous topics when you have time. Consider the sequence of
topics in this course as the following:

1. Study the first figure (CPU, Memory, I/O) in the compendia so you
   understand that the CPU has registers and loads instructions and data
   from memory, and to load these instructions and data we need to reach
   them in memory with __addresses__.
2. Then study what an operating system is, what a process is, and how an operating
   system runs with special privileges. Chapters 2, 4, 5 and 6 in the text
   book. 
3. How you choose to study the rest of the topics has a natural sequence,
   but you can also choose this sequence yourself (in other words: choose
   between these after you have completed item 1 and 2 above):
    * Scheduling
    * Address spaces and paging BEFORE Memory management
    * Threads and locks BEFORE Condition variables, semaphores, concurrency
    * I/O and HDD/SSD BEFORE File systems
    * Virtual Machines and Containers
    * Operating System Security 

Also remember that the text book has a nice summary at the end of each
chapter. If you think the chapter text is difficult or you do not have time
to read the entire chapter, then skip to the end and read the summary. 

## Weekly schedule

| Week | Topic (Chapters) | Additional Info |
| ---- | -------------    | --------------- |
| 34   | **Course intro, SkyHiGh, Linux and C-programming**<br/>[Lab tutorials](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.md#user-content-lab-tutorials) |  |
| 35   | **Computer architecture ([compendia](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.pdf) chp 1, [potensregning](https://gitlab.com/erikhje/iikos/-/blob/master/potensregning.pdf))** <br/>ALU, CU, I/O, bus, MMU, controller, firmware, bit vs Byte, data vs instruction, instruction set, micro architecture, register, AX/BX/CX/DX/SP/BP/IP/IR/FLAG/PSW, address, interrupt, interrupt handler/routine, stack/push/pop, context switch, assembly directive/label, clock-speed/frequency/period, Hz, pipeline, micro-operations, out-of-order execution, branch prediction, superscalar, SMT/hyperthreading, von Neumann-bottle neck, spatial/temporal locality, cache line, write through/back cache<br/>[Review questions and problems](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.md#user-content-review-questions-and-problems)<br/>Lectures:<br/>[kapittel 1 del 1 CPU Minne IO](https://youtu.be/3DSdyGxk5XQ?si=Au5FXQUv3qrM9NxV)<br/>[kapittel 1 del 2 CPU klokke minne stack](https://youtu.be/vC4jmYxw2vM?si=XYJ60gxqGoi9IXlo)<br/>[kapittel 1 del 3 Assembly](https://youtu.be/ZnCsgedmOgE?si=wnh4WNET72d-8ynN)<br/>[kapittel 1 del 4 Avansert CPU Hyperthreading](https://youtu.be/3mzSbBGUxTQ?si=--hN3p2syeJmXzXB)<br/>[kapittel 1 del 5 Cache](https://youtu.be/p-2aSGFvuHE?si=jbsxiES44xgJywS4) | <!-- Video-lecture: [course introduction](https://youtu.be/Q_xsxQcLQ6Y), [computer architecture part 3](https://youtu.be/gHlDm6aBBfY)--> |
| 36 (ONLY TUESDAY!)  | **Introduction and processes ([compendia](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.pdf) chp 2, textbook chp [2](http://pages.cs.wisc.edu/~remzi/OSTEP/intro.pdf), [4](http://pages.cs.wisc.edu/~remzi/OSTEP/cpu-intro.pdf))** <br/>process, process API, thread/multi-threaded, process states (ready/running/blocked), PCB, process list/table, address space, file, design goals, timesharing, batch, soft/hard real-time, service/user process, CPU/IO/Memory-bound processes, GNU, POSIX, bit/Byte, KB/MB/GB/TB/PB/EB, ms/us/ns, gcc<br/>[Review questions and problems](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.md#user-content-review-questions-and-problems-1), [Lab tutorials](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.md#user-content-lab-tutorials-1)<br/>Lectures:<br/>[kapittel 2 del 1 OSTEP ORG kapittel 2](https://youtu.be/DpLT1om1KaQ?si=44mKwEx4rJJ6DAuZ)<br/>[kapittel 2 del 2 OSTEP ORG kapittel 4](https://youtu.be/3Xf4c8KJXrc?si=cT8Z7bRPVLq_u7Sf)<br/>[Intro pointers in C](https://youtu.be/4cZn-5i31sI) | [MANDATORY TASK 1](https://gitlab.com/erikhje/iikos/-/blob/master/oblig/2024-1.md) |
| 37   | **System calls ([compendia](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.pdf) chp 3, textbook chp [5](http://pages.cs.wisc.edu/~remzi/OSTEP/cpu-api.pdf), [6](http://pages.cs.wisc.edu/~remzi/OSTEP/cpu-mechanisms.pdf), (see also this: [page 1-8](http://cslibrary.stanford.edu/102/PointersAndMemory.pdf)))** <br/>fork, copy-on-write, exec, wait, signal, limited direct execution, instructions/system calls/commands, kernel mode, user mode, mode switch/transition, preemptive multitasking, trap table (interrupt vector table), sync/async interrupts, software/exception/hardware interrupt, timer interrupt, Process ID (PID), (call) stack, kernel stack, privileged operation/instruction, cooperative vs preemptive (timer interrupt)<br/>[Review questions and problems](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.md#user-content-review-questions-and-problems-2), [Lab tutorials](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.md#user-content-lab-tutorials-2)<br/>Lectures:<br/>[kapittel 3 del 1 OSTEP ORG kapittel 5](https://youtu.be/uXMq2F_ZIj0?si=ycqRFRZvhPPKhbiy)<br/>[kapittel 3 del 2 OSTEP ORG kapittel 6](https://youtu.be/BBJdt8Wok_4?si=Ynvy12aj61lXUu1N) | |
| 38   | **Scheduling ([compendia](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.pdf) chp 4, textbook chp [7](http://pages.cs.wisc.edu/~remzi/OSTEP/cpu-sched.pdf), [8](http://pages.cs.wisc.edu/~remzi/OSTEP/cpu-sched-mlfq.pdf), [9.1](http://pages.cs.wisc.edu/~remzi/OSTEP/cpu-sched-lottery.pdf), [10.1, 10.3](http://pages.cs.wisc.edu/~remzi/OSTEP/cpu-sched-multi.pdf)** <br/>clock interrupt, preemptive vs non-preemptive, turnaround time, response time, workload, FCFS/FIFO, convoy effect, SJF, STCF, Round Robin, time quantum, jiffie, MLFQ, priority levels, boost, dynamic priority, fair-share/lottery scheduling, CPU-pinning/affinity, gang-/co-scheduling<br/>[Review questions and problems](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.md#user-content-review-questions-and-problems-3), [Lab tutorials](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.md#user-content-lab-tutorials-3)<br/>Lectures:<br/>[kapittel 4 del 1 OSTEP ORG kapittel 7](https://youtu.be/S0Z5z6Bysjc)<br/>[kapittel 4 del 2 OSTEP ORG kapittel 8](https://youtu.be/F8WzWs9dwfs)<br/>[kapittel 4 del 3 OSTEP ORG kapittel 9.1 10.1 10.3](https://youtu.be/LN3d5PbhF-w) | <!--**TBL-1: Computer Architecture, Processes and System calls**<br/>(in the lab/exercises hours)--> **DEADLINE MANDATORY TASK 1** |
| 39   | **Address spaces and paging ([compendia](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.pdf) chp 5, textbook chp [14](http://pages.cs.wisc.edu/~remzi/OSTEP/vm-api.pdf), [16.1, 16.4](http://pages.cs.wisc.edu/~remzi/OSTEP/vm-segmentation.pdf), [17.1](http://pages.cs.wisc.edu/~remzi/OSTEP/vm-freespace.pdf), [18](http://pages.cs.wisc.edu/~remzi/OSTEP/vm-paging.pdf)) (browse chp [13](http://pages.cs.wisc.edu/~remzi/OSTEP/vm-intro.pdf) and [15](http://pages.cs.wisc.edu/~remzi/OSTEP/vm-mechanism.pdf))** <br/>address space, kernel/user space, multiprogramming, stack/heap memory, malloc(), free(), valgrind, translation, relocation, base and bound/limit registers, segments, free list, bitmap, external/internal fragmentation, paging, offset, page, page frame, virtual/physical address, page table, page table entry (PTE), present/absent bit, referenced bit, modified/dirty bit, memory trace<br/>[Review questions and problems](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.md#user-content-review-questions-and-problems-4), [Lab tutorials](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.md#user-content-lab-tutorials-4)<br/>Lectures:<br/>[kapittel 5 del 1 OSTEP ORG kapittel 13 14](https://youtu.be/spsm0OFMayk)<br/>[kapittel 5 del 2 OSTEP ORG kapittel 15 16.1 16.4 17.1](https://youtu.be/g6LUf6kUju8)<br/>[kapittel 5 del 3 OSTEP ORG kapittel 18](https://youtu.be/Ul2F9WH2g-o)<br/>[Adresser og bits](https://www.youtube.com/watch?v=QMisRDN3-Ds) | [MANDATORY TASK 2](https://gitlab.com/erikhje/iikos/-/blob/master/oblig/2024-2.md) |
| 40 (ALSO GUEST LECTURE WED 11:15 S206!)  | **Memory management ([compendia](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.pdf) chp 6, textbook chp [19 (skip 19.7)](http://pages.cs.wisc.edu/~remzi/OSTEP/vm-tlbs.pdf), [20 (skip 20.2)](http://pages.cs.wisc.edu/~remzi/OSTEP/vm-smalltables.pdf), [21](http://pages.cs.wisc.edu/~remzi/OSTEP/vm-beyondphys.pdf), [22](http://pages.cs.wisc.edu/~remzi/OSTEP/vm-beyondphys-policy.pdf)) (browse chp [23.2](http://pages.cs.wisc.edu/~remzi/OSTEP/vm-complete.pdf))** <br/>TLB, hit/miss, hit rate, temporal/spatial locality, TLB entry, ASID, multi-level page table, PTBR, PDBR, CR3, inverted page table, swap, page fault, minor/major page fault, optimal/fifo/random/LRU/clock page replacement, demand-paging vs pre-paging/pre-fetching, thrashing, working set, hugepages<br/>[Review questions and problems](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.md#user-content-review-questions-and-problems-5), [Lab tutorials](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.md#user-content-lab-tutorials-5)<br/>Lectures:<br/>[kapittel 6 del 1 OSTEP ORG kapittel 19](https://youtu.be/gJzuai25zf0)<br/>[kapittel 6 del 2 OSTEP ORG kapittel 20](https://youtu.be/OabefgG4ObM)<br/>[kapittel 6 del 3 OSTEP ORG kapittel 21](https://youtu.be/KrzD1cWcDdE)<br/>[kapittel 6 del 4 OSTEP ORG kapittel 22 23 2](https://youtu.be/QGRPYqNszTY) | Extra video to help you understand memory: [How a Clever 1960s Memory Trick Changed Computing (Laurie Kirk, Reverse engineer at Microsoft)](https://www.youtube.com/watch?v=vc79sJ9VOqk) |
| 41   | **Threads and locks ([compendia](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.pdf) chp 7, textbook chp [26](http://pages.cs.wisc.edu/~remzi/OSTEP/threads-intro.pdf), [27](http://pages.cs.wisc.edu/~remzi/OSTEP/threads-api.pdf), [28.1-9, 28.12-13](http://pages.cs.wisc.edu/~remzi/OSTEP/threads-locks.pdf))** <br/>PCB vs TCB, single- vs multi-thread, pthread create/join, atomicity, critical section, race condition, deterministic, mutual exclusion, mutex lock, test-and-set, xchg, compare-and-swap, cmpxchg, lock prefix, spin/busy waiting, spin or switch, yield, two-phase lock<br/>[Review questions and problems](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.md#user-content-review-questions-and-problems-6), [Lab tutorials](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.md#user-content-lab-tutorials-6)<br/>Lectures:<br/>[kapittel 7 del 1 OSTEP ORG kapittel 26](https://youtu.be/PqxVMJ0RU3w)<br/>[kapittel 7 del 2 OSTEP ORG kapittel 27](https://youtu.be/XEwsT-ASy94)<br/>[kapittel 7 del 3 OSTEP ORG kapittel 28](https://youtu.be/4xiEcalgHhs)<br/>[kapittel 7 del 4 OSTEP ORG kapittel deadlock](https://youtu.be/wbmx0VCA_Co) | <!--**TBL-2: Scheduling, Address spaces, Paging and Memory management**<br/>(in the lab/exercises hours)--> **DEADLINE MANDATORY TASK 2** |
| 42 | **Condition variables, semaphores, concurrency ([compendia](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.pdf) chp 8, textbook chp [30](http://pages.cs.wisc.edu/~remzi/OSTEP/threads-cv.pdf), [31](http://pages.cs.wisc.edu/~remzi/OSTEP/threads-sema.pdf))** <br/>pthread cond_wait/cond_signal, producer-consumer, semaphore, sem_wait (down), sem_post (up), binary semaphore, ordering/synchronizing semaphore, reader-writer, starvation, dining philosophers, barrier, monitor, deadlock<br/>[Review questions and problems](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.md#user-content-review-questions-and-problems-7), [Lab tutorials](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.md#user-content-lab-tutorials-7)<br/>Lectures:<br/>[kapittel 8 del 1 OSTEP ORG kapittel 30](https://youtu.be/Lfib5lZPqDs)<br/>[kapittel 8 del 2 OSTEP ORG kapittel 31](https://youtu.be/81uqGapahT8)<br/>[kapittel 8 del 3 OSTEP ORG kapittel barrier java atomicint](https://youtu.be/Wftx3NTnnRQ) | [MANDATORY TASK 3](https://gitlab.com/erikhje/iikos/-/blob/master/oblig/2024-3.md) |
| 43   | **I/O and HDD/SSD ([compendia](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.pdf) chp 9, textbook chp [36.1-36.7](http://pages.cs.wisc.edu/~remzi/OSTEP/file-devices.pdf), [37.1-37.4](http://pages.cs.wisc.edu/~remzi/OSTEP/file-disks.pdf), [RAID](https://commons.wikimedia.org/w/index.php?title=Redundant_array_of_independent_disks&oldid=148689461), [44.1-6, 44.10-12](http://pages.cs.wisc.edu/~remzi/OSTEP/file-ssd.pdf))** <br/> Memory and I/O buses/interconnect, PCI/USB/SATA, micro-controller, I/O device, programmed I/O, interrupt-based I/O, DMA, I/O instructions (isolated I/O), memory-mapped I/O, I/O stack, block device, storage stack, block addresses, sector, HDD, platter, surface, spindle, RPM, track, cylinder, disk arm, disk head, seek time, rotational delay, SSD, SLC/MLC/TLC, NAND flash, flash translation layer, trim, write amplification, wear levelling, RAID 0/1/5, iops, sequential/random read/write<br/>[Review questions and problems](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.md#user-content-review-questions-and-problems-8), [Lab tutorials](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.md#user-content-lab-tutorials-8)<br/>Lectures:<br/>[kapittel 9 del 1 OSTEP ORG kapittel 36 1 36 7](https://youtu.be/Wj9mvAs3KPk)<br/>[kapittel 9 del 2 OSTEP ORG kapittel 37 1 37 4](https://youtu.be/PS3Dm4ifQcs)<br/>[kapittel 9 del 3   OSTEP ORG kapittel 44 1 6 44 10 12](https://youtu.be/JvOhiSj3M38)<br/>[kapittel 9 del 4 RAID IOPS](https://youtu.be/vVXCCJb_gDw) | |
| 44   | **File systems ([compendia](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.pdf) chp 10, textbook [39.1-4, 39.7-18](http://pages.cs.wisc.edu/~remzi/OSTEP/file-intro.pdf), [40](http://pages.cs.wisc.edu/~remzi/OSTEP/file-implementation.pdf), [42.1-2](http://pages.cs.wisc.edu/~remzi/OSTEP/file-journaling.pdf))** <br/>inode, open(), read(), write(), close(), STDIN/STDOUT/STDERR, file descriptor, fsync, metadata, strace, link/unlink, mkdir(), opendir(), readdir(), closedir(), rmdir(), hard link, symbolic link, permission bits (rwx), SetUID, SetGID, sticky bit, chmod(), chown(), mkfs, mount, inode/data bitmap, metadata, superblock, single/double/triple indirect pointers/addressing, extents, EXT, page cache, sleuthkit, fsck, journalling, idempotent<br/>[Review questions and problems](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.md#user-content-review-questions-and-problems-9), [Lab tutorials](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.md#user-content-lab-tutorials-9)<br/>Lectures:<br/>[kapittel 10 del 1 OSTEP ORG kapittel 39](https://youtu.be/6AOXaJBhWQU)<br/>[kapittel 10 del 2 OSTEP ORG kapittel 40](https://youtu.be/PbTK_rdEMfA)<br/>[kapittel 10 del 3   OSTEP ORG kapittel 40 eksempel](https://youtu.be/OJ4owhD6TdI)<br/>[kapittel 10 del 4 OSTEP ORG kapittel 42 1 2](https://youtu.be/mxOyYmox74Y)  | <!--**TBL-3: Threads, Locks, Concurrency and I/O**<br/>(in the lab/exercises hours) **MANDATORY multiple choice test in Blackboard this week! opens Oct 30th, closes Nov 5th 23:59. Three attempts, must have 20 correct out of 30 questions to qualify for the exam** --> **DEADLINE MANDATORY TASK 3** |
| 45   | **Virtual Machines and Containers ([compendia](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.pdf) chp 11, [Hardware Virtualization: the Nuts and Bolts](https://gitlab.com/erikhje/iikos/-/blob/master/anandtech.pdf) (stop before "Standardization please!"))** <br/>unikernel, sensitive/privileged instructions, trap-and-emulate, binary translation, basic blocks, paravirtualization, hardwaresupported virtualization, vmx/svm/ept/npt/vpid/asid/vt-d, shadow/guest/physical page table, page walk, CR3, cgroup, namespaces, union mounts, Docker<br/>[Review questions and problems](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.md#user-content-review-questions-and-problems-10), [Lab tutorials](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.md#user-content-lab-tutorials-10)<br/>Lectures:<br/>[kapittel 11 del 1 Virtuelle maskiner](https://youtu.be/TYLT1I00fik)<br/>[kapittel 11 del 2 Containere](https://youtu.be/d8ZgQB7hVZ8) | |
| 46 (ONLY WEDNESDAY in S206!)  | **Operating System Security ([compendia](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.pdf) chp 12, textbook [53](http://pages.cs.wisc.edu/~remzi/OSTEP/security-intro.pdf))** <br/>security policy, CIA, secure systems design principles, reference monitor, identification, authentication, authorization, capability, ACL, ACE, access token, security descriptor, privileges, MAC, DAC, mandatory integrity control, DACL, SACL, integrity levels, SID, secure attention sequence, UAC, namespace virtualization, UID/GID, sudo, buffer overflow, heap spraying, nop sled, stack canary, DEP, NX, return-to-libc, ASLR<br/>[Review questions and problems](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.md#user-content-review-questions-and-problems-11), [Lab tutorials](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.md#user-content-lab-tutorials-11)<br/>Lectures:<br/>[kapittel 12 del 1 OSTEP ORG kapittel 53](https://youtu.be/yDQNxLPtJWA)<br/>[kapittel 12 del 2 Aksesskontroll 1](https://youtu.be/fEZzS80H0x0)<br/>[kapittel 12 del 3 Aksesskontroll 2](https://youtu.be/InP7G9aWQNE)<br/>[kapittel 12 del 4   Beskyttelse mot programvaresårbarheter 1](https://youtu.be/51GILU-o2QI)<br/>[kapittel 12 del 4   Beskyttelse mot programvaresårbarheter 2](https://youtu.be/nYD8-772Ciw) | [Heat-stack](https://gitlab.com/erikhje/heat-mono/-/blob/master/single_linux_16.04.yaml) [Return-to-libc](http://cse.sustech.edu.cn/faculty/~zhangfw/19fa-cs315/labs/lab12-return-to-libc.pdf) (see lab tutorial in compendia chp 12) |
| 47 (S206 Tuesday and Wednesday) | Repetition<br/>exam info (exam on Nov 26th, no aids allowed, three hours) |  |

<!---
IF WE TRY TO SQUEEZE IN COMPUTER ARCHITECTURE IT WILL BE TOO TIGHT:

| Week | Topic (Chapters) | Exercises | Additional Resources |
| ---- | -------------    | --------- | -------------------- |
| 34   | Arch, X86 Assembly, Registers | | |
| 35   | CPU-details, Multicore/SMT | | |
| 36   | Cache, I/O, Interrupts | | |
| 37   | Intro, Process (2, 4) |  |  |
| 38   | SysCalls (5, 6)|  |  |
| 39   | Scheduling (7, 8, 9.1, 10.1, 10.3) |  |  |
| 40   | Address spaces/translation, C (13, 14, 15) |  |  |
| 41   | Segmentation and paging (16, 17, 18, 19) |  |  |
| 42   | Memory management (20, 21, 22) |  |  |
| 43   | Threads, locks, conditional variables (26, 27, 28, 29, 30) |  |  |
| 44   | Semaphores, concurrency problems (31, 32) |  |  |
| 45   | I/O, RAID (36, 37.1-37.4, 38)  |  |  |
| 46   | File systems, FSCK/journalling, SSD (39, 40, 42, 44) |  |  |
| 47   | Virtualization and containers (separate document) |  |  |
--->
