[[_TOC_]]

## About this Compendium

This is NOT A TEXTBOOK. This a just a collection of lecture notes
together with weekly exercises and labs.

Special thanks go out to the following individuals who provided valuable
feedback: Mariusz Nowostawski, Christopher Frantz, Ivar Farup, Ernst
Gunnar Gran, Eigil Obrestad, Donn Morrison and Slobodan Petrovic.

# Login, Files and Directories

## What Is the Shell (TLCL chp 1)

  - Shell

  - Terminal

  - Prompt

  - date, cal, df, free

  - History  
    arrow up and down, Also TAB-completion and CTRL-r

## Navigation (TLCL chp 2)

Note: Directory (NO: katalog) and folder (NO: mappe) are synonyms in our
context.

  - Root directory

  - Current working directory

  - Parent directory

  - Home directory

  - pwd, ls, cd

  - Absolute vs Relative path

  - cd .

  - cd ..

  - cd -

  - cd \(\sim\)  
    (same as `cd` without any arguments, but note that \(\sim\) is
    useful when giving path names relative to your home directory)

  - ls -a

  - (EXTRA MATERIAL:) tree, pushd, popd

## Exploring the System (TLCL chp 3)

  - Command Option(s) Argument(s)

  - ls -ltr \(\sim\)

  - file, less

  - Configuration file

  - Script

  - File System: home, root, etc, bin, tmp, media

  - Symbolic link

## Manipulating Files and Directories (TLCL chp 4)

  - Wildcards: \* ?

  - cp, mkdir, mv, rm, ln -s  
    (you can skip the topic “Hard links”)

  - nano  
    simplest text editor, se menu on the bottom of the screen, notice
    `^` means press CTRL-key, e.g. to save and exit press CTRL and X
    together

  - (EXTRA MATERIAL:) The editor vi: insert/command mode

## About the Lab Exercises

Note: text in `UPPER_CASE` should be replaced by you when you type the
example commands.

### If not a student at NTNU

![image](/home/erikhje/office/kurs/netcli/01-filesdir/tex/net-setup-fig-nonNTNU.pdf)

### If you are a student at NTNU

![image](/home/erikhje/office/kurs/netcli/01-filesdir/tex/net-setup-fig.pdf)

## Review questions and problems

1.  What is the relationship between a Shell and a Terminal?
    
    1.  A Terminal and a Shell is the same thing.
    
    2.  A Terminal interprets commands and a Shell is a program we use
        to get access to the Terminal.
    
    3.  A Terminal is what we use to terminate a Shell.
    
    4.  A Shell interprets commands and a Terminal is a program we use
        to get access to the Shell.

2.  Which one of the following examples of file paths is an absolute
    pathname?
    
    1.  `./work/courses`
    
    2.  `work/courses`
    
    3.  `/home/ubuntu/work/courses`
    
    4.  `../ubuntu/work/courses`

3.  How do you create a symbolic link from `/var/www/html` to your
    current working directory?
    
    1.  `ln -s /var/www/html .`
    
    2.  `ln /var/www/html .`
    
    3.  `ln /var/www/html /.`
    
    4.  `ln -s /var/www/html ~`

4.  Copy the file `/etc/services` to your home directory. Rename it to
    `commonports`

5.  Change directory to `/tmp`. Create a directory `mytmp` in `/tmp`.
    Move the file `commonports` from the previous exercise to the newly
    created `mytmp` directory. Verify that the file is there and no
    longer in your home directory.

6.  Create a directory `backups`. Copy all files from another directory
    to the `backups` directory by copying recursively and preserving all
    time stamps.

7.  (OPTIONAL) Open the file `mypasswd` with `vi`. Delete the first five
    lines of the file, save and quit.

## Lab tutorials

1.  **Commands and command line history**.
    
    1.  Try all the commands in chapter one of .
    
    2.  Use the up and down arrows to see how you can repeat commands
        from your history.
    
    3.  Do  
        `cat .bash_history`  
        log out and log back in again, and repeat  
        `cat .bash_history`
    
    4.  Google to try and find out the default maximum number of
        commands saved into the `.bash_history` file.

2.  **Navigation**.
    
    1.  Try all the commands in chapter two of .

3.  **Exploring the system.**.
    
    1.  Try all the commands in chapter three of . You do not have to
        know the entire file system hierarchy in table 3.4 but know that
        you can look it up when needed. *Also try to use TAB completion
        to quickly do the following commands*  
        
            cd /usr/local/share/ca-certificates/
            ls -l /var/lib/ubuntu-release-upgrader/release-upgrade-available
            cd /usr/share/bash-completion/helpers
            cd /usr/include/linux/misc/
    
    2.  Make sure you understand how to search in a text file that you
        are viewing with `less` (`/mysil` (the ’`/`’ is the "search
        command") will search for the text `mysil` and `n` will show you
        the next occurrence). This is useful because this is how it
        works in a couple of other widely used text editors and viewers
        as well.
    
    3.  Note: find out how you can copy and paste. This works in
        different ways dependent on which terminal you are using and if
        you are using Linux, Mac or Windows. Ask teacher if unsure.
    
    4.  Do `ls -l` in your home directory. Do you have any symbolic
        links there?

4.  **Manipulating files and directories**.
    
    1.  Try all the commands in chapter four of .
    
    2.  Do the following
        
            cd
            cp /etc/passwd .
            ls -l passwd
            cp -a /etc/passwd .
            ls -l passwd
        
        What does the -a option do to the cp command?
    
    3.  In chapter five of , read carefully (and do the commands) in the
        section about `man`

5.  **(OPTIONAL EXERCISE) Editing a file with vi**.
    
    1.  Read and do all the commands on pages 141-145 (the first five
        pages) in chapter 12 of .

6.  **(OPTIONAL EXERCISE) Keep your session: Screen and tmux**.
    
    `screen` is the old classic “terminal multiplexer” while `tmux` is a
    newer version with some more features. If you know the basics of one
    of these, you can keep your session (everything you are currently
    doing) even if you log out or lose your network connection.
    
    1.  Log in to the server (replace this command if not a
        NTNU-student)  
        `ssh YOUR_USERNAME@login.stud.ntnu.no`
    
    2.  Start a tmux session and run a command in it  
        `tmux`  
        `ls`
    
    3.  Leave the tmux session by typing CTRL-b then d (for detach).
    
    4.  Log out of the server  
        `exit`
    
    5.  Log in to the server (replace this command if not a
        NTNU-student)  
        `ssh YOUR_USERNAME@login.stud.ntnu.no`
    
    6.  Go back into your tmux session  
        `tmux a`
    
    7.  See how you can do other things, e.g. create multiple windows
        and scroll up and down at <https://tmuxcheatsheet.com/>.

# Redirections and Expansions

## Redirection (TLCL chp 6)

  - Two types of output

  - STDIN, STDOUT, STDERR

  - Create, Append, Read  
    `>, >>, <`

  - STDERR to STDOUT  
    `2>&1`

  - /dev/null

  - cat

  - CTRL-d

  - Pipelines

  - Important difference  
    `| vs >`

  - Filters: sort, uniq, wc, grep

  - grep -irv

  - head/tail -n -f

  - tee

## Expansion (TLCL chp 7)

  - Expansion  
    “Each time we type a command and press the Enter key, bash performs
    several substitutions upon the text before it carries out our
    command. We have seen a couple of cases of how a simple character
    sequence, for example `*`, can have a lot of meaning to the shell.
    The process that makes this happen is called expansion” (, page 68).

  - $((2+2))

  - {A,B,C}, {01..12}  
    Loop with  
    `for i in {01..05}; do echo "Nr $i";done`

  - printenv

  - $( )  
    Loop with  
    `for i in $(seq 1 5); do echo "Nr $i";done`  
    or more relevant  
    `for f in /bin/a*; do echo "Owner of $f is $(stat -c %U $f)";done`

  - VARIABLES

  - Double quotes

  - Single quotes

  - Escaping a character

## Review questions and problems

1.  Only one of these makes sense, which one?
    
    1.  `$ command | file`
    
    2.  `$ file > command`
    
    3.  `$ command > file`
    
    4.  `$ file | command`

2.  What is the output of the command `echo "$(2 + 2)"` ?
    
    1.  `4`
    
    2.  `2 + 2`
    
    3.  `$(2 + 2)`
    
    4.  `2: command not found`

3.  Which character to you put in front of `$` if you want `echo "$a"`
    to actually print `$a` (and not expand it to the variable `a`) ?
    
    1.  `\`
    
    2.  `` ` ``
    
    3.  `|`
    
    4.  `/`

4.  Create a file containing just the number `1` with the command  
    `echo 1 > test.txt`
    
    1)  Append the numbers `2` and `3` to the file so the output will be
        
            $ cat test.txt 
            1
            2
            3
    
    2)  Create the file again with just the number `1`, but now append
        the numbers `2` and `3` using two commands but without a line
        break, and then use a third command to add the line break (to
        the end of the file) so the output will be
        
            $ cat test.txt 
            1
            23
    
    Hint: `man echo`

5.  As a normal user (not root), do `find /etc/ -name host*` (try this
    command now to see what you get as output, before moving on)
    
    1)  Redirect STDOUT from this command to a file `test.dat` (view the
        result with `cat test.dat`)
    
    2)  Redirect STDERR from this command to a file `test.dat` (view the
        result with `cat test.dat`)
    
    3)  Redirect STDOUT and STDERR from this command to a file
        `test.dat` (view the result with `cat test.dat`)

6.  Write a command to recursively (meaning “include all
    subdirectories”) search for the word `hostname` in all files under
    the `/etc` directory. You should redirect standard error to
    `/dev/null` so you don’t see any "Permission denied" messages. Hint:
    use `grep` and see `man grep` to find out how to recursively search.

7.  Use the `cat` command to redirect output to a file `test.txt`, enter
    the text `mysil` and use `CTRL-D` to send the EOF (end of file)
    signal. Verify that the file now has the word mysil:
    
        $ cat test.txt 
        mysil

8.  Download a file with names:  
    `wget https://folk.ntnu.no/erikhje/navn.dat`
    
    1)  Use `wc` to count the number of lines and words in the file.
    
    2)  Sort the contents of the file.
    
    3)  Show each name in the file only once.
    
    4)  Count the number of lines containing `Emil`
    
    5)  Show a list of the most frequent occurring names like this
        (hint: `man uniq`):
        
        ``` 
              5 Emil
              3 Frida
              2 Per Arne
              2 Emma
              2 Ella
              1 William
              1 Sofie
        etc
        ```

9.  Write a command pipeline to list files in a directory hierarchy
    (e.g. your home directory) that have the name pattern `*.txt` and
    sort them by filesize.

10. Write a command to show the first five lines of the file
    `~/.bashrc`.

11. Write a command pipeline to write lines 21-24 of `~/.bashrc` to a
    new file `strange.tmp`.

12. Create the following directory structure using `mkdir -p` and brace
    expansion.
    
        .
        |--A
        |  |--0
        |  |--1
        |
        |--B
        |  |--0
        |  |--1
        |
        |--C
           |--0
           |--1
    
    Verify that it has been created with the `ls` command by using the
    option for recursive listing (also try the `tree` command). Delete
    the entire directory structure you created with a single command.

13. Store the output from `whoami` in a variable `$me` and use `echo` to
    print "I am YOUR\_USERNAME" (e.g. "I am erikhje"). Repeat this
    exercise but now without using a variable.

14. Write a command pipeline that will print the number of txt-files in
    your current working directory, e.g.  
    `There are 7 txt-files here.`  
    if there are 7 txt-files in your current working directory.

15. Write three command lines to output the number of lines that contain
    the letter `A`, `B` and `C` respectively in `/usr/share/dict/words`
    to a file `wordstat.dat`. After you have executed the three command
    lines you should be able to do this
    
        $ cat wordstat.dat
        A: 1436
        B: 1477
        C: 1636
    
    Hint: you need to use parameter substitution (`$( )`).  
    Note: if you do not have the file `words` you can download it  
    `wget https://folk.ntnu.no/erikhje/words`

## Lab tutorials

1.  **Redirection and pipelines.**
    
    1.  Try all the commands in chapter six of .
    
    2.  try the command `find` and do an internet search for  
        `linux find examples`

2.  **Expansion.**
    
    1.  Try all the commands in chapter seven of . On page 76, remember
        that you can create a file like this  
        `echo Mysil > "two words.txt"`
    
    2.  Do `man bash` and search in this man-page for the headline
        “ARITHMETIC EVALUATION”, and try the following commands
        
            i=5
            ((i++))
            echo $i
    
    3.  What was the `i` ? It was a variable, note that you need to
        avoid spaces when assigning variables otherwise Bash will
        interpret it as a command with options/arguments. Try the
        following commands
        
            a = Mysil
            echo $a
            a=Mysil
            echo $a
            a=$(pwd)
            echo $a

3.  **Extracting a number from a table.**
    
    We can show lines matching patterns with `grep`. What if we want to
    show only the third column from the lines? Try the following
    commands
    
        grep cpu0 /proc/stat
        grep cpu0 /proc/stat | awk '{print $3}'
        num=$(grep cpu0 /proc/stat | awk '{print $3}')
        echo $num
        ((num++))
        echo $num
    
    Note the command `awk '{print $3}'` to show column three.

# Permissions and Processes

## Permissions (TLCL chp 9)

  - Ownership  
    user, group, everyone

  - id

  - rwxrwxrwx

  - chmod

  - Number systems  
    binary (01), octal (0..7), decimal (0..9), hexadecimal (0..9A..F)

  - SetUID

  - sudo -s vs -i  
    (Note: Ubuntu disables logins to the root account)

  - chown, chgrp

  - adduser

## Processes (TLCL chp 10)

  - Multitasking and multiuser

  - Process ID (PID)

  - Process state

  - ps aux, top, htop

  - background  
    `nano &` or `nano`, CTRL-z, `bg`

  - foreground  
    `fg`

  - jobs

  - CTRL-c

  - kill

  - reboot, shutdown

  - pstree  
    (check out `tree` for files as well)

  - PATH  
    When you run a command, if it’s not a Bash builtin (like `cd`), Bash
    will look for the program in the directories listed in the
    environment variable PATH, and execute the first matching program it
    encounters.

## Review questions and problems

1.  Connect the commands in each column to the correct keyword.
    
    |  | `chmod` | `ps` | `ln -s` | `sudo` | `kill` | `id` |
    | :- | :------ | :--- | :------ | :----- | :----- | :--- |
    |  |         |      |         |        |        |      |
    |  |         |      |         |        |        |      |
    |  |         |      |         |        |        |      |
    |  |         |      |         |        |        |      |
    |  |         |      |         |        |        |      |
    |  |         |      |         |        |        |      |
    

2.  Connect the octal number in each column to the correct file
    permissions.
    
    |  | `640` | `777` | `700` | `755` | `600` | `444` |
    | :- | :---- | :---- | :---- | :---- | :---- | :---- |
    |  |       |       |       |       |       |       |
    |  |       |       |       |       |       |       |
    |  |       |       |       |       |       |       |
    |  |       |       |       |       |       |       |
    |  |       |       |       |       |       |       |
    |  |       |       |       |       |       |       |
    

3.  Write a command to show the line containing your username in
    `/etc/passwd` (note: this doesn’t work on *login* since that server
    is configured to use centralized user accounts instead of the local
    ones in `/etc/passwd`). Add a pipeline to the command to show only
    the fifth column where columns are separated with `:` (hint: `man
    cut`).

4.  Write commands for the following
    
    1.  Create a file `a.txt`
    
    2.  Change permissions on `a.txt` to `r--r-----`
    
    3.  Try to delete `a.txt` (redo steps above if you are able to
        delete it)
    
    4.  Change permissions on `a.txt` to `rw-------`
    
    5.  Add a user `mysil`
    
    6.  Change the owner of `a.txt` to be `mysil`
    
    7.  Try to delete `a.txt` (redo steps above if you are able to
        delete it)
    
    8.  Move `a.txt` to `/tmp/`
    
    9.  Try to delete `/tmp/a.txt`

5.  Write a command pipeline to show the three processes owned by you
    that uses the most RAM/physical memory (the RSS column you get from
    the `ps` command).

6.  Start a process (e.g. `bash` or `nano`) in the background. Remove it
    with the `kill` command (make sure you kill the right process by
    using the correct PID).

## Lab tutorials

1.  **Permissions.**
    
    1.  Try all the commands in chapter nine of . Some commands (like
        `id`) you should try on both *login* and *VM*. Some of the
        commands for changing ownership and permissions might involve
        several user accounts. On *VM*, you have root access and can
        create new accounts, e.g.  
        `sudo adduser mysil`
    
    2.  See if any of the programs available to you have the SetUID-bit
        set  
        `ls -l /usr/bin | grep rws`

2.  **Processes.**
    
    1.  Try all the commands in chapter ten of . When you log in from
        remote, you will probably not be able to run GUI-programs like
        `xlogo` so replace `xlogo` with `nano` in the examples.
    
    2.  Do `pstree -p | less` Why isn’t `init` PID 1 like the book says?
        Google and/or ask teacher.

3.  **The environment variable PATH.**
    
    1.  Do `echo $PATH` on *VM*, then do
        
            VM$ mkdir ~/bin
            VM$ cat > ~/bin/ls
            #!/bin/bash
            echo "I love seeing files and directories!" 
            /bin/ls
            CTRL-d
            VM$ chmod +x ~/bin/ls
        
        log out and back in again, run `ls`, what happens and why?
    
    *Remember to always test your code. Linux commands and Bash are very
    powerful and it is easy to make mistakes. Make a note of the
    following references for future use:
    [ShellCheck](https://www.shellcheck.net/) and [Bash
    Pitfalls](https://mywiki.wooledge.org/BashPitfalls).*

# PowerShell

## PowerShell

##### PowerShell

  - See [separate document on
    PowerShell](https://gitlab.com/erikhje/dcsg1005/-/blob/master/powershell.md).
    Read quickly through the first section, READ CAREFULLY FROM [Objects
    and
    Get-Member](https://gitlab.com/erikhje/dcsg1005/-/blob/master/powershell.md#objects-and-get-member)
    up to [if, comparison, tests,
    "branching"](https://gitlab.com/erikhje/dcsg1005/-/blob/master/powershell.md#if-comparison-tests-branching).

### Aliases

Based on what we have covered in the first three chapters about Linux,
table [4.1](#tab:aliases) lists the relevant aliases for some of the
commands you probably already know.

<div id="tab:aliases">

|               |                  |  |  |
| :------------ | :--------------- | :- | :- |
| **Linux**     | **PowerShell**   |  |  |
| `pwd`         | `Get-Location`   |  |  |
| `ls`          | `Get-ChildItem`  |  |  |
| `cd`          | `Set-Location`   |  |  |
| `cp`          | `Copy-Item`      |  |  |
| `mv`          | `Move-Item`      |  |  |
| `rm`          | `Remove-Item`    |  |  |
| `cat`         | `Get-Content`    |  |  |
| `sort`        | `Sort-Object`    |  |  |
| `tee`         | `Tee-Object`     |  |  |
| `echo`        | `Write-Output`   |  |  |
| `ps`          | `Get-Process`    |  |  |
| `kill`        | `Stop-Process`   |  |  |
| `(wc)`        | `Measure-Object` |  |  |
| `(head/tail)` | `Select-Object`  |  |  |

Relevant aliases.

</div>

## Review questions and problems

1.  Find out which PowerShell cmdlet are mapped to the aliases `cd`,
    `echo`, `cat`, `cp`, `rm` and `sort`. Try all these six cmdlets.

2.  Store the output of  
    `(Resolve-DnsName ftp.uninett.no).IP4Address`  
    in a variable. Pipe the variable to `Get-Member` to see which
    properties and methods the variable (which is an object like
    "everything" is in PowerShell) has.
    
    1.  Print the Length of the variable.
    
    2.  Use the `split()`-method to split the four octets of the IP
        address so they are printed each on a line by themselves.

3.  Write a command line which recursively outputs all directories in
    your home directory. It should not list files, only directories
    (hint: `Get-Help -Online Get-ChildItem`).

4.  Use `Get-Member` to list all properties and methods in the the
    objects you get from `Get-ChildItem`. Pipe to `more` to page-by-page
    (by hitting space) or line-by-line (by hitting enter). Repeat with
    `Get-Process` instead of `Get-ChildItem`.

5.  Piping to `Select-Object -Property *` is many times useful to see
    all the properties an object has (remember: ordinary output on
    screen only shows a subset of all the properties).
    
    1.  List all the user accounts on your computer with
        `Get-LocalUser`, choose a user and see all the properties for
        that account: when was the user last logged on?
    
    2.  List all the files in your home directory with `Get-ChildItem
        $env:HOMEPATH`, choose a file and see all the properties for
        that file: when was the file last accessed?

6.  Write a PowerShell command line that will list all files in the
    directory `C:\Windows` that are larger than 10KB. Then add to the
    pipeline:
    
    1.  Make the output sorted based on file size (Length)
    
    2.  Make the output show only the three largest files
    
    3.  Make the output show only file name, file size, last access time
        and last write time

7.  Write a PowerShell command line that will show how much space is
    used by all the files in all the directories (including all
    subdirectories) in `$env:HOMEPATH`. The output should be in MB
    (MegaBytes).

8.  Write a command line which outputs all processes which have the
    property `StartTime` within the last hour.

9.  Write a command line which prints the name of all directories (from
    your current directory) which contain at least 10
    files/subdirectories.

10. Create a variable `$golf` with the value `Viktor Hovland`. Use
    `Write-Output` to print "My golf hero Viktor Hovland" using this
    variable. Advanced: Now try to use the `Split()` method of the
    variable to print only "My golf hero Hovland" (hint: you can use the
    `Split()`-method just like this without any arguments).

11. (OPTIONAL EXERCISE) Using
    [splatting](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_splatting),
    write a command line which recursively outputs all files (not
    directories, only files) in  
    `C:\Windows\System32\LogFiles` and also have the parameter
    ErrorAction set to SilentlyContinue.

## Lab tutorials

1.  Work through all the integrated exercises (indented text with a left
    "bar") from [Objects and
    Get-Member](https://gitlab.com/erikhje/dcsg1005/-/blob/master/powershell.md#objects-and-get-member)
    up to [if, comparison, tests,
    "branching"](https://gitlab.com/erikhje/dcsg1005/-/blob/master/powershell.md#if-comparison-tests-branching)
    in the [PowerShell
    tutorial](https://gitlab.com/erikhje/dcsg1005/blob/master/powershell.md).
